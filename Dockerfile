FROM php:7.1-apache
COPY ./ /var/www/html/
EXPOSE 87

CMD echo "ServerName www.php-docker.com" >> /etc/apache2/apache2.conf
CMD ["/usr/sbin/apache2ctl", "-D", "FOREGROUND"]
